

import sys
from transforms import mirror, grayscale, blur, sepia, aumentar_brillo, disminuir_brillo
from images import read_img, write_img, create_blank
def main():
    image = read_img(sys.argv[1])

    if sys.argv[2] == "mirror":
        image_trans = mirror(image)
    elif sys.argv[2] == "grayscale":
        image_trans = grayscale(image)
    elif sys.argv[2] == "blur":
        image_trans = blur(image)
    elif sys.argv[2] == "sepia":
        image_trans = sepia(image)
    elif sys.argv[2] == "aumentar_brillo":
        image_trans = aumentar_brillo(image)
    elif sys.argv[2] == "disminuir_brillo":
        image_trans = disminuir_brillo(image)
    else:
        print("Transformación no válida")

    file_name = sys.argv[1].replace(".", "_trans.")
    write_img(image_trans, file_name)

if __name__ == '__main__':
    main()
