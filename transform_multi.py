import sys
from transforms import mirror, grayscale, blur, change_colors, rotate, shift, crop, filter, sepia, aumentar_brillo, disminuir_brillo
from images import read_img, write_img

def transformaciones(image, transforms):
    for transform in transforms:
        nombre_filtro = transform[0]
        args = transform[1:]
        if nombre_filtro == 'mirror':
            image = mirror(image)
        elif nombre_filtro == 'grayscale':
            image = grayscale(image)
        elif nombre_filtro == 'blur':
            image = blur(image)
        elif nombre_filtro == 'change_colors':
            color_original = [tuple(map(int, color.split(','))) for color in args[0].split(':')]
            color_nuevo = [tuple(map(int, color.split(','))) for color in args[1].split(':')]
            image = change_colors(image, color_original, color_nuevo)
        elif nombre_filtro == 'rotate':
            image = rotate(image, args[0])
        elif nombre_filtro == 'shift':
            horizontal, vertical = map(int, args)
            image = shift(image, horizontal, vertical)
        elif nombre_filtro == 'crop':
            x, y, width, height = map(int, args)
            image = crop(image, x, y, width, height)
        elif nombre_filtro == 'filter':
            r, g, b = map(float, args)
            image = filter(image, r, g, b)
        elif sys.argv[2] == "sepia":
            image = sepia(image)
        elif sys.argv[2] == "aumentar_brillo":
            image = aumentar_brillo(image)
        elif sys.argv[2] == "disminuir_brillo":
            image = disminuir_brillo(image)
    return image

def main():
    args = sys.argv[1:]
    image_file = args[0]
    image = read_img(image_file)

    transforms = []
    current_transform = []
    for arg in args[1:]:
        if arg in ['mirror', 'grayscale', 'blur', 'change_colors', 'rotate', 'shift', 'crop', 'filter', 'sepia', 'aumentar_brillo', 'disminuir_brillo']:
            if current_transform:
                transforms.append(current_transform)
            current_transform = [arg]
        else:
            current_transform.append(arg)
    if current_transform:
        transforms.append(current_transform)

    transformed = transformaciones(image, transforms)

    file_name = image_file.replace(".", "_trans.")
    write_img(transformed, file_name)

if __name__ == '__main__':
    main()
