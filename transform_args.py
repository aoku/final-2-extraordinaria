import sys
from transforms import change_colors, rotate, shift, crop, filter
from images import read_img, write_img, create_blank

def main():

    image_file = sys.argv[1]
    imagen = read_img(image_file)

    if sys.argv[2] == "change_colors":
        color_original = [tuple(map(int, color.split(','))) for color in sys.argv[3].split(':')]
        color_nuevo = [tuple(map(int, color.split(','))) for color in sys.argv[4].split(':')]
        image_trans = change_colors(imagen, color_original, color_nuevo)

    elif sys.argv[2] == "rotate":
        direccion = sys.argv[3]
        image_trans = rotate(imagen, direccion)

    elif sys.argv[2] == "shift":
        horizontal = int(sys.argv[3])
        vertical = int(sys.argv[4])
        image_trans = shift(imagen, horizontal, vertical)

    elif sys.argv[2] == "crop":
        x = int(sys.argv[3])
        y = int(sys.argv[4])
        width = int(sys.argv[5])
        height = int(sys.argv[6])
        image_trans = crop(imagen, x, y, width, height)

    elif sys.argv[2] == "filter":
        r = float(sys.argv[3])
        g = float(sys.argv[4])
        b = float(sys.argv[5])
        image_trans = filter(imagen, r, g, b)

    else:
        print(f"Transformación no válida.")
        return

    file_name = sys.argv[1].replace(".", "_trans.")
    write_img(image_trans, file_name)


if __name__ == '__main__':
    main()
