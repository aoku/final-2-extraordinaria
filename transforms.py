from images import *
from images import read_img, write_img, create_blank, size


def mirror(image: dict) -> dict:

    (width, height) = size(image)
    image_mirrored = create_blank(width, height)
    for fila in range(height):
        for columna in range(width):
            image_mirrored['pixels'][fila*width+columna] = image['pixels'][(height-1-fila)*width+columna]

    return image_mirrored


def grayscale(image: dict) -> dict:

    width, height = size(image)
    img_gris = create_blank(width, height)

    for i in range(height * width):
        r, g, b = image['pixels'][i]
        valor_gris = (r + g + b) // 3
        img_gris['pixels'][i] = (valor_gris, valor_gris, valor_gris)

    return img_gris

def blur(image: dict) -> dict:

    width, height = size(image)
    img_difuminada = create_blank(width, height)

    for i in range(height * width):
        pixel_vecino = []
        y, x = i // width, i % width
        for dy in [-1, 0, 1]:
            for dx in [-1, 0, 1]:
                if dy == dx == 0:
                    continue
                ny, nx = y + dy, x + dx
                if 0 <= ny < height and 0 <= nx < width:
                    pixel_vecino.append(image['pixels'][ny * width + nx])

        avg_r = sum(pixel[0] for pixel in pixel_vecino) // len(pixel_vecino)
        avg_g = sum(pixel[1] for pixel in pixel_vecino) // len(pixel_vecino)
        avg_b = sum(pixel[2] for pixel in pixel_vecino) // len(pixel_vecino)
        img_difuminada['pixels'][i] = (avg_r, avg_g, avg_b)

    return img_difuminada


def change_colors(image: dict,
                  original: list[tuple[int, int, int]],
                  change: list[tuple[int, int, int]]) -> dict:
    width, height = size(image)
    imagen_cambio_color = create_blank(width, height)

    for i in range(height * width):
        r, g, b = image['pixels'][i]
        new_color = (r, g, b)
        for j in range(len(original)):
            if (r, g, b) == original[j]:
                new_color = change[j]
                break
        imagen_cambio_color['pixels'][i] = new_color

    return imagen_cambio_color

def rotate(image: dict, direccion: str) -> dict:
    width, height = size(image)
    imagen_rotada = create_blank(height, width)

    if direccion == "right":
        for i in range(height * width):
            y, x = i // width, i % width
            new_y, new_x = width - 1 - x, y
            imagen_rotada['pixels'][new_y * height + new_x] = image['pixels'][i]
    elif direccion == "left":
        for i in range(height * width):
            y, x = i // width, i % width
            new_y, new_x = x, height - 1 - y
            imagen_rotada['pixels'][new_y * height + new_x] = image['pixels'][i]
    else:
        raise ValueError("La dirección debe ser 'right' o 'left'")

    return imagen_rotada

def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    width, height = size(image)
    nuevo_ancho = width + abs(horizontal)
    nuevo_alto = height + abs(vertical)
    imagen_desplazada = create_blank(nuevo_ancho, nuevo_alto)

    for i in range(height * width):
        y, x = i // width, i % width
        new_y = y + vertical
        new_x = x + horizontal

        if 0 <= new_y < nuevo_alto and 0 <= new_x < nuevo_ancho:
            imagen_desplazada['pixels'][new_y * nuevo_ancho + new_x] = image['pixels'][i]

    return imagen_desplazada


def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
    orig_width, orig_height = size(image)
    imagen_recortada = create_blank(width, height)

    for i in range(height * width):
        crop_y, crop_x = i // width, i % width
        orig_y, orig_x = y + crop_y, x + crop_x

        if 0 <= orig_y < orig_height and 0 <= orig_x < orig_width:
            imagen_recortada['pixels'][i] = image['pixels'][orig_y * orig_width + orig_x]

    return imagen_recortada

def filter(image: dict, r: float, g: float, b: float) -> dict:
    width, height = size(image)
    imagen_filtrada = create_blank(width, height)

    for i in range(height * width):
        orig_r, orig_g, orig_b = image['pixels'][i]
        new_r = min(int(orig_r * r), 255)
        new_g = min(int(orig_g * g), 255)
        new_b = min(int(orig_b * b), 255)
        imagen_filtrada['pixels'][i] = (new_r, new_g, new_b)

    return imagen_filtrada


def sepia(image: dict) -> dict:
    width, height = size(image)
    img_sepia = create_blank(width, height)

    for i in range(height * width):
        r, g, b = image['pixels'][i]

        tr = int(0.393 * r + 0.769 * g + 0.189 * b)
        tg = int(0.349 * r + 0.686 * g + 0.168 * b)
        tb = int(0.272 * r + 0.534 * g + 0.131 * b)

        tr = min(max(tr, 0), 255)
        tg = min(max(tg, 0), 255)
        tb = min(max(tb, 0), 255)

        img_sepia['pixels'][i] = (tr, tg, tb)

    return img_sepia


def aumentar_brillo(image: dict) -> dict:
    width, height = size(image)
    brillo_aumentado = create_blank(width, height)

    factor = 1.5

    for i in range(height * width):
        r, g, b = image['pixels'][i]

        r = int(r * factor)
        g = int(g * factor)
        b = int(b * factor)

        brillo_aumentado['pixels'][i] = (r, g, b)

    return brillo_aumentado


def disminuir_brillo(image: dict) -> dict:
    width, height = size(image)
    brillo_disminuido = create_blank(width, height)

    factor = 0.5

    for i in range(height * width):
        r, g, b = image['pixels'][i]

        r = int(r * factor)
        g = int(g * factor)
        b = int(b * factor)

        brillo_disminuido['pixels'][i] = (r, g, b)

    return brillo_disminuido








